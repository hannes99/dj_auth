#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase, override_settings
from django.http import HttpResponse

from dj_auth.widgets import ObjectFilterValuesWidget


@override_settings(DJ_AUTH={'content_type_exclude': (),
                            'content_type_include': ('auth.user',),
                            'global_fields_exclude': (),
                            'content_type_fields_include': {},
                            'content_type_fields_exclude': {},
                            }
                   )
class test_ObjectFilterValuesdWidget(TestCase):

    def test_attrs(self):
        wigdet = ObjectFilterValuesWidget(attrs={'content_type': 1, 'field_name': 'content_type'})
        html = HttpResponse(wigdet.render('content_type', '[1, ]'))
        self.assertContains(html, '<div id="values_block">')

    def test_empty_attrs(self):
        wigdet = ObjectFilterValuesWidget(attrs={})
        html = HttpResponse(wigdet.render('group', '[1, ]'))
        self.assertContains(html, '<div id="values_block">')

    def test_attrs_True(self):
        wigdet = ObjectFilterValuesWidget(attrs={'content_type': 1, 'field_name': 'content_type'})
        html = HttpResponse(wigdet.render('content_type', 'True', ))
        self.assertContains(html, '<div id="values_block">')
