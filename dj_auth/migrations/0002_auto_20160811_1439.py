# -*- coding: utf-8 -*-
# Generated by Django 1.9.9 on 2016-08-11 12:39
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dj_auth', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='objectfilter',
            name='content_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='dj_auth_objectfilter_content_type', to='contenttypes.ContentType', verbose_name='Class'),
        ),
        migrations.AlterField(
            model_name='objectfilter',
            name='filter_type',
            field=models.SmallIntegerField(choices=[(0, 'Equals'), (1, 'Not Equals')], verbose_name='Filter Type'),
        ),
        migrations.AlterField(
            model_name='objectfilter',
            name='users',
            field=models.ManyToManyField(related_name='dj_auth_objectfilter_users', to=settings.AUTH_USER_MODEL, verbose_name='User'),
        ),
    ]
